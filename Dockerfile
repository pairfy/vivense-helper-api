FROM openjdk:8-jdk-alpine
EXPOSE 5000
ARG JAR_FILE=target/vivense-helper-api-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} vivense-helper-api.jar
ENTRYPOINT ["java","-jar","/vivense-helper-api.jar"]
