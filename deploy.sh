mvn clean

# fat spring boot jar
mvn package -DskipTests=true

# send release jar to server via scp like ftp
scp ./target/vivense-helper-api-0.0.1-SNAPSHOT.jar root@vivense.pairfy.team:/releases/

# run spring boot application from remote ssh
ssh root@vivense.pairfy.team 'kill -9 $(lsof -t -i:5000)'
ssh root@vivense.pairfy.team 'java -jar /releases/vivense-helper-api-0.0.1-SNAPSHOT.jar &'