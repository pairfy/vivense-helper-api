package com.vivense.vivensehelperapi.util;

public class PsikolojikPrice {

    public static double execute(double kampanyaliSatisFiyati) {

        if(kampanyaliSatisFiyati % 10 == 0) {
            kampanyaliSatisFiyati -= 1.0; // sonu 0 ise 1 çıkarmaca psikolojik savaş
        }

        return kampanyaliSatisFiyati;

    }
}
