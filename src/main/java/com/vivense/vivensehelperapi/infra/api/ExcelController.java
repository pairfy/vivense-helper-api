package com.vivense.vivensehelperapi.infra.api;

import com.vivense.vivensehelperapi.util.ExcelUtil;
import com.vivense.vivensehelperapi.util.PsikolojikPrice;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@RestController
@RequestMapping("excel")
public class ExcelController {



    @PostMapping("v2/indirim-orani/{indirimOrani}")
    public void excelv2(@RequestParam(required = false) String campaignStartDate,
                        @RequestParam String campaignEndDate,
                        HttpServletResponse response,
                        @PathVariable int indirimOrani,
                        @RequestPart(required = true) MultipartFile file,
                        @RequestParam(required = false, defaultValue = "0.90") Double üreticiKargoMarj,
                        @RequestParam(required = false, defaultValue = "0.80") Double vivenseKargoMarj) throws IOException { // 10

        Sheet sheet = ExcelUtil.sheetFromWorkbookByPositionMultiPartFile(file, 0);

        // each row cell set if business occured
        for (Row row : sheet) {
            // manipulateThisRow(row);
            String id = row.getCell(0).toString();
            if (id.equals("ID")) continue; // header hack

            // campaignStartDate setlemece kullanıcı girerse
            if (campaignStartDate != null) {
                Cell cell = row.createCell(10);
                cell.setCellValue(campaignStartDate);
            }

            // campaignEndDate setlemece
            Cell cell = row.createCell(11);
            cell.setCellValue(campaignEndDate);

            String name = row.getCell(1).toString();
            double alisFiyati = row.getCell(7).getNumericCellValue(); // sola yatmamalı.
            double kampanyaliAlisFiyati = (alisFiyati * (100 - indirimOrani)) / 100.;

            Cell kampanyaAlisFiyati = row.createCell(14);

            if (kampanyaAlisFiyati == null) {
                kampanyaAlisFiyati.setCellValue(kampanyaliAlisFiyati);
            } else {
                kampanyaAlisFiyati.setCellValue(kampanyaliAlisFiyati);
            }

            String evli = row.getCell(3).toString();
            String operationType = row.getCell(19).toString();

            // marjing
            if (operationType.equalsIgnoreCase("Tedarikçi kargo") || operationType.equalsIgnoreCase("Üretici kargo/nakliyat") || operationType.equalsIgnoreCase("Üretici kargo")) {
                double kampanyaAlisFiyatiCellValue = kampanyaAlisFiyati.getNumericCellValue();

                double kampanyaliSatisFiyati = Math.ceil(kampanyaAlisFiyatiCellValue / üreticiKargoMarj); // 60

                Cell kampanyaliSatisFiyatiCell = row.createCell(15);

                if (kampanyaliSatisFiyatiCell == null) {
                    row.getCell(15).setCellValue(PsikolojikPrice.execute(kampanyaliSatisFiyati));
                } else {
                    kampanyaliSatisFiyatiCell.setCellValue(PsikolojikPrice.execute(kampanyaliSatisFiyati));
                }
            } else { // vivense kargo marjing
                double kampanyaAlisFiyatiCellValue = kampanyaAlisFiyati.getNumericCellValue();

                double kampanyaliSatisFiyati = Math.ceil(kampanyaAlisFiyatiCellValue / vivenseKargoMarj);

                Cell kampanyaliSatisFiyatiCell = row.createCell(15);

                if (kampanyaliSatisFiyatiCell == null) {
                    kampanyaliSatisFiyatiCell.setCellValue(PsikolojikPrice.execute(kampanyaliSatisFiyati));
                } else {
                    kampanyaliSatisFiyatiCell.setCellValue(PsikolojikPrice.execute(kampanyaliSatisFiyati));
                }
            }

            // marjing ile setledikten sonra
            double kampanyaliSatisFiyati = row.getCell(15).getNumericCellValue();
            double psfFiyati = row.getCell(17).getNumericCellValue();

            if (psfFiyati <= kampanyaliSatisFiyati) { // psf küçükse sorun var çözelim
                kampanyaliSatisFiyati += 50;
                row.getCell(17).setCellValue(kampanyaliSatisFiyati);
            }
        }

        // http response sending
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            sheet.getWorkbook().write(bos); // this is modified workbook.
        } finally {
            bos.close();
        }

        byte[] modifiedExcel = bos.toByteArray();

        response.setHeader("Content-Disposition", "attachment;filename=myExcel.xls");
        response.setContentType("application/vnd.ms-excel"); // todo: produced
        response.setContentLength(modifiedExcel.length);

        OutputStream os = response.getOutputStream();

        try {
            os.write(modifiedExcel, 0, modifiedExcel.length);
        } catch (Exception excp) {
            excp.printStackTrace();
        } finally {
            os.close();
        }
        System.out.println("Sending excel to client as a http response please download the response");
    }

    /*
    @ApiOperation(value = "This method is used to get the current date.", hidden = true)
    // from postman logic
    @PostMapping("indirim-orani/{indirimOrani}")
    public void excel(HttpServletRequest request, HttpServletResponse response, @PathVariable int indirimOrani) throws IOException { // 10

        Sheet sheet = ExcelUtil.sheetFromWorkbookByPosition(request, 0);

        // each row cell set if business occured
        for (Row row : sheet) {
            // manipulateRow(row);
            String id = row.getCell(0).toString();
            if (id.equals("ID")) continue; // header hack
            String name = row.getCell(1).toString();
            double alisFiyati = row.getCell(7).getNumericCellValue();
            double kampanyaliAlisFiyati = (alisFiyati * (100 - indirimOrani)) / 100.;

            Cell kampanyaAlisFiyati = row.createCell(14);

            if (kampanyaAlisFiyati == null) {
                kampanyaAlisFiyati.setCellValue(kampanyaliAlisFiyati);
            }
            else {
                kampanyaAlisFiyati.setCellValue(kampanyaliAlisFiyati);
            }

            String evli = row.getCell(3).toString();
            String operationType = row.getCell(19).toString();

            // marjing
            if (operationType.equalsIgnoreCase("Üretici kargo/nakliyat") || operationType.equalsIgnoreCase("Üretici kargo")) {
                double kampanyaAlisFiyatiCellValue = kampanyaAlisFiyati.getNumericCellValue();

                double kampanyaliSatisFiyati = Math.ceil(kampanyaAlisFiyatiCellValue / 0.95);

                Cell kampanyaliSatisFiyatiCell = row.createCell(15);

                if (kampanyaliSatisFiyatiCell == null) {
                    row.getCell(15).setCellValue(kampanyaliSatisFiyati);
                }
                else {
                    kampanyaliSatisFiyatiCell.setCellValue(kampanyaliSatisFiyati);
                }
            }
            else {
                double kampanyaAlisFiyatiCellValue = kampanyaAlisFiyati.getNumericCellValue();

                double kampanyaliSatisFiyati = Math.ceil(kampanyaAlisFiyatiCellValue / 0.82);

                Cell kampanyaliSatisFiyatiCell = row.createCell(15);

                if (kampanyaliSatisFiyatiCell == null) {
                    kampanyaliSatisFiyatiCell.setCellValue(kampanyaliSatisFiyati);
                }
                else {
                    kampanyaliSatisFiyatiCell.setCellValue(kampanyaliSatisFiyati);
                }
            }
        }

        // http response sending
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            sheet.getWorkbook().write(bos); // this is modified workbook.
        } finally {
            bos.close();
        }

        byte[] modifiedExcel = bos.toByteArray();

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "filename=\"THE FILE NAME\"");
        response.setContentLength(modifiedExcel.length);

        OutputStream os = response.getOutputStream();

        try {
            os.write(modifiedExcel , 0, modifiedExcel.length);
        } catch (Exception excp) {
            excp.printStackTrace();
        } finally {
            os.close();
        }
        System.out.println("Sending excel to client as a http response please download the response");
    }
     */

}
