package com.vivense.vivensehelperapi.infra.api;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionAdviceController {

    @ExceptionHandler(Exception.class)
    public String generalExceptionOccurred(Exception ex)
    {
        return "Excelde hata var diye düşündük:" + ex.getMessage();
    }

}
