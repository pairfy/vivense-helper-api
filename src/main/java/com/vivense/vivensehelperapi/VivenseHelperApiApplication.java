package com.vivense.vivensehelperapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Scanner;

@SpringBootApplication
public class VivenseHelperApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(VivenseHelperApiApplication.class, args);
    }
}
