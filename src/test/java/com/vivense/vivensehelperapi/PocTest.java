package com.vivense.vivensehelperapi;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class PocTest {

    @Test
    void kampanyali_satis_fiyati_sonu_0_ise_1_azaltmaca() {
        double kampanyaliSatisFiyati = 60.00;

        if (kampanyaliSatisFiyati % 10 == 0) {
            kampanyaliSatisFiyati -= 1;
        }

        Assertions.assertThat(kampanyaliSatisFiyati).isEqualTo(59.00);
    }

    @Test
    void kampanyali_satis_fiyati_sonu_0_ise_1_azaltmaca_() {
        double kampanyaliSatisFiyati = 50.00;

        if (kampanyaliSatisFiyati % 10 == 0) {
            kampanyaliSatisFiyati -= 1;
        }

        Assertions.assertThat(kampanyaliSatisFiyati).isEqualTo(49.00);
    }
}
