package com.vivense.vivensehelperapi.infra.api;

import com.vivense.vivensehelperapi.util.PsikolojikPrice;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PsikolojikPriceTest {

    @Test
    void it_should_decrease_by_one_from_kampanyaliSatisFiyati() {

        // given
        double kampanyaliSatisFiyati = 60.0;

        // when
        double actualKampanyaliSatisFiyati = PsikolojikPrice.execute(kampanyaliSatisFiyati);

        // then
        Assertions.assertEquals(59.0, actualKampanyaliSatisFiyati);
    }

    @Test
    void it_should_decrease_by_one_from_kampanyaliSatisFiyati_other_sample() {

        // given
        double kampanyaliSatisFiyati = 50.0;

        // when
        double actualKampanyaliSatisFiyati = PsikolojikPrice.execute(kampanyaliSatisFiyati);

        // then
        Assertions.assertEquals(49.0, actualKampanyaliSatisFiyati);
    }
}